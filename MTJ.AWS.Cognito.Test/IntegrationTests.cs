using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MTJ.AWS.Cognito.Test
{
    [TestClass]
    public class IntegrationTests
    {
        private const string USERNAME = "jmarkyston";
        private const string PASSWORD = "Admin112!";

        private AwsCognitoClient Client = new AwsCognitoClient();

        private void AssertResponse<T>(CognitoResponse<T> response) where T : AmazonWebServiceResponse
        {
            Assert.AreEqual(response.Data.HttpStatusCode, System.Net.HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task Register()
        {
            AssertResponse(await Client.Register("jmarkyston@gmail.com", USERNAME, PASSWORD));
        }

        [TestMethod]
        public async Task ConfirmRegistration()
        {
            AssertResponse(await Client.ConfirmRegistration(USERNAME, "235352"));
        }

        [TestMethod]
        public async Task AuthenticateAndGetUser()
        {
            CognitoResponse<InitiateAuthResponse> response = await Client.Authenticate(USERNAME, PASSWORD);
            AssertResponse(response);
            AssertResponse(await Client.GetUser(response.Data.AuthenticationResult.AccessToken));
        }

        [TestMethod]
        public async Task ResendConfirmation()
        {
            AssertResponse(await Client.ResendConfirmation(USERNAME));
        }

        [TestMethod]
        public async Task ForgotPassword()
        {
            AssertResponse(await Client.ForgotPassword(USERNAME));
        }

        [TestMethod]
        public async Task ConfirmForgotPassword()
        {
            AssertResponse(await Client.ConfirmForgotPassword(USERNAME, PASSWORD, "315399"));
        }
    }
}