﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Cognito
{
    public class CognitoResponse<T>
    {
        public T Data { get; set; }
        public CognitoError Error { get; set; }

        public CognitoResponse(T data)
        {
            Data = data;
        }
        public CognitoResponse(CognitoError error)
        {
            Error = error;
        }
        public CognitoResponse(T data, CognitoError error)
        {
            Data = data;
            Error = error;
        }
    }
}
