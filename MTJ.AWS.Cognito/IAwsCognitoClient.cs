﻿using Amazon.CognitoIdentityProvider.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTJ.AWS.Cognito
{
    public interface IAwsCognitoClient
    {
        Task<CognitoResponse<SignUpResponse>> Register(string email, string username, string password);
        Task<CognitoResponse<ConfirmSignUpResponse>> ConfirmRegistration(string username, string confirmationCode);
        Task<CognitoResponse<InitiateAuthResponse>> Authenticate(string username, string password);
        Task<CognitoResponse<ResendConfirmationCodeResponse>> ResendConfirmation(string username);
        Task<CognitoResponse<ForgotPasswordResponse>> ForgotPassword(string username);
        Task<CognitoResponse<ConfirmForgotPasswordResponse>> ConfirmForgotPassword(string username, string password, string confirmationCode);
        Task<CognitoResponse<GetUserResponse>> GetUser(string accessToken);
    }
}
