﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Cognito
{
    public class CognitoError
    {
        private Dictionary<CognitoErrorCode, string> Messages = new Dictionary<CognitoErrorCode, string>()
        {
            { CognitoErrorCode.CodeMismatchException, "Invalid verification code provided, please try again." },
            { CognitoErrorCode.NotAuthorizedException, "Incorrect username or password." },
            { CognitoErrorCode.UsernameExistsException, "User already exists." },
            { CognitoErrorCode.UserNotConfirmedException, "User is not confirmed." },
            { CognitoErrorCode.ExpiredCodeException, "Confirmation code expired." }
        };

        public CognitoErrorCode ErrorCode { get; set; }
        public string Message { get; set; }

        public CognitoError() { }
        public CognitoError(CognitoErrorCode errorCode)
        {
            ErrorCode = errorCode;
            Message = Messages[errorCode];
        }
        public CognitoError(string genericExceptionMessage)
        {
            ErrorCode = CognitoErrorCode.GenericException;
            Message = genericExceptionMessage;
        }
    }
}
