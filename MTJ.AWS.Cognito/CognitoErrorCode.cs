﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Cognito
{
    public enum CognitoErrorCode
    {
        UnknownException,
        GenericException,
        UsernameExistsException,
        NotAuthorizedException,
        CodeMismatchException,
        UserNotConfirmedException,
        ExpiredCodeException
    }
}
