﻿using MTJ.AWS.Core;
using System;
using System.Collections.Generic;
using System.Text;
using Amazon.CognitoIdentityProvider;
using System.Threading.Tasks;
using Amazon.CognitoIdentityProvider.Model;
using System.Linq;
using Amazon.Runtime;
using Microsoft.Extensions.Configuration;

namespace MTJ.AWS.Cognito
{
    public class AwsCognitoClient : AwsClientBase<IAmazonCognitoIdentityProvider>, IAwsCognitoClient
    {
        private string ClientId => Environment.GetEnvironmentVariable("CognitoClientId") ?? Configuration.GetSection("AWS").GetSection("CognitoClientId").Value;

        public AwsCognitoClient() { }
        public AwsCognitoClient(IConfiguration configuration): base(configuration) { }

        private async Task<CognitoResponse<T>> Execute<T>(Func<Task<T>> func)
        {
            T data = default;
            CognitoError error = null;
            try { data = await func(); }
            catch (Exception x)
            {
                if (x is AmazonServiceException)
                {
                    string value = ((AmazonServiceException)x).ErrorCode;
                    CognitoErrorCode errorCode = CognitoErrorCode.UnknownException;
                    Enum.TryParse(value, out errorCode);
                    if (errorCode == CognitoErrorCode.UnknownException)
                        throw new Exception($"Unknown Cognito exception: {x.Message}");
                    else
                        error = new CognitoError(errorCode);
                }
                else
                    throw x;
            }
            return new CognitoResponse<T>(data, error);
        }

        public async Task<CognitoResponse<SignUpResponse>> Register(string email, string username, string password)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.SignUpAsync(new SignUpRequest()
                {
                    ClientId = ClientId,
                    Username = username,
                    Password = password,
                    UserAttributes = new List<AttributeType>()
                    {
                        new AttributeType()
                        {
                            Name = "email",
                            Value = email
                        }
                    }});
            });
        }

        public async Task<CognitoResponse<ConfirmSignUpResponse>> ConfirmRegistration(string username, string confirmationCode)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.ConfirmSignUpAsync(new ConfirmSignUpRequest()
                {
                    ClientId = ClientId,
                    ConfirmationCode = confirmationCode,
                    Username = username
                });
            });
        }

        public async Task<CognitoResponse<InitiateAuthResponse>> Authenticate(string username, string password)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.InitiateAuthAsync(new InitiateAuthRequest()
                {
                    ClientId = ClientId,
                    AuthFlow = AuthFlowType.USER_PASSWORD_AUTH,
                    AuthParameters = new Dictionary<string, string>()
                    {
                        { "USERNAME", username },
                        { "PASSWORD", password }
                    }
                });
            });
        }

        public async Task<CognitoResponse<ResendConfirmationCodeResponse>> ResendConfirmation(string username)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.ResendConfirmationCodeAsync(new ResendConfirmationCodeRequest()
                {
                    ClientId = ClientId,
                    Username = username
                });
            });
        }

        public async Task<CognitoResponse<ForgotPasswordResponse>> ForgotPassword(string username)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.ForgotPasswordAsync(new ForgotPasswordRequest()
                {
                    ClientId = ClientId,
                    Username = username
                });
            });
        }

        public async Task<CognitoResponse<ConfirmForgotPasswordResponse>> ConfirmForgotPassword(string username, string password, string confirmationCode)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.ConfirmForgotPasswordAsync(new ConfirmForgotPasswordRequest()
                {
                    ClientId = ClientId,
                    Username = username,
                    Password = password,
                    ConfirmationCode = confirmationCode
                });
            });
        }

        public async Task<CognitoResponse<GetUserResponse>> GetUser(string accessToken)
        {
            return await Execute(async () =>
            {
                return await ServiceClient.GetUserAsync(new GetUserRequest()
                {
                    AccessToken = accessToken
                });
            });
        }
    }
}